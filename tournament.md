# Final Tournament Results

'-' denotes a protocol that fails to reach the goal.

'r' denotes a protocol that will be reevaluated.

Note: Even with fixed random seed, race conditions between threads may lead to nondeterministic execution.

## Map 1: Platform, No Balloon Teleportation

Weight: 1/3

| Team Name                  | 1st Balloon Pop | 2nd Balloon Pop | Home    | Rank |
| ---                        | ---             | ---             | ---     | ---  |
| Drone Appetit              | 68.25           | 129.14          | 215.35  | 6    |
| General Line               | 25.41           | 39.95           | 57.28   | 4    |
| Quadsadilla                | - (a)           | -               | -       | 7    |
| Rolling Drones             | 12.92           | 25.30           | 36.70   | 1    |
| TEAM_NAME_HERE             | 14.51           | 24.20           | 39.08   | 2    |
| Todd's Quad Squad          | 58.17           | 90.88           | 106.71  | 5    |
| Unscented Kwadrotor Fliers | 19.73           | 32.61           | 45.95   | 3    |

Results were obtained using the random seed 57.

(a) Autonomy protocol freezes shortly after execution.

## Map 2: Forest, Balloon Teleportation < 60 s

Weight: 1/3

| Team Name                  | 1st Balloon Pop | 2nd Balloon Pop | Home    | Rank |
| ---                        | ---             | ---             | ---     |  --- |
| Drone Appetit              | 149.86          | 216.06          | - (b)   | 6    |
| General Line               | 15.66           | 16.41           | 93.30   | 4    |
| Quadsadilla                | - (c)           | -               | -       | 7    |
| Rolling Drones             | 12.29           | 53.08           | 91.80   | 3    |
| TEAM_NAME_HERE             | 21.49           | 39.75           | 50.23   | 1    |
| Todd's Quad Squad          | 22.59           | 66.37           | 139.67  | 5    |
| Unscented Kwadrotor Fliers | 19.73           | 31.81           | 53.86   | 2    |

Results were obtained using the random seed 6.

(b) Segfault in autonomy protocol at 290s.

(c) Autonomy protocol segfaults at roughly 20s.

## Map 3: Temple, Balloon Teleportation < 60s

Weight: 1/3

| Team Name                  | 1st Balloon Pop | 2nd Balloon Pop | Home    | Rank |
| ---                        | ---             | ---             | ---     | ---  |
| Drone Appetit              | 120.64          | 209.59          | 284.03  | 4    |
| General Line               | 33.86           | - (d)           | -       | 6    |
| Quadsadilla                | (e)             | -               | -       | 7    |
| Rolling Drones             | 32.11           | 48.96           | 92.79   | 3    |
| TEAM_NAME_HERE             | 27.02           | 42.62           | 73.02   | 2    |
| Todd's Quad Squad          | 28.45           | 55.71           | - (f)   | 5    |
| Unscented Kwadrotor Fliers | 31.97           | 45.00           | 67.10   | 1    |

Results were obtained using the random seed 31.

(d) Autonomy protocol actually segfaults before popping first balloon, but dispatched trajectory continues to run successfully pops balloon.

(e) Autonomy protocol segfaults at roughly 20s.

(f) Quadrotor collides with obstacles roughly half of runs.


## Final Results

| Team Name                  | Heat 1 | Heat 2 | Heat 3  | Weighted Rank | Final Rank |
| ---                        | ---    | ---    | ---     | ---           | ---        |
| Drone Appetit              | 6      | 6      | 4       | 5.33          | 6          |
| General Line               | 4      | 4      | 6       | 4.66          | 4          |
| Quadsadilla                | 7      | 7      | 7       | 7             | 7          |
| Rolling Drones             | 1      | 3      | 3       | 2.33          | 3          |
| *TEAM_NAME_HERE*           | 2      | 1      | 2       | 1.67          | *1*        |
| Todd's Quad Squad          | 5      | 5      | 5       | 5             | 5          |
| Unscented Kwadrotor Fliers | 3      | 2      | 1       | 2             | 2          |
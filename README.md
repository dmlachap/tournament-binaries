# Tournament Binaries

Note: To test these binaries, you may need to add the path of libyaml-cpp.so.0.6 to your LD_LIBRARY_PATH.

See the instructions below:
```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/aerial-robotics/Libraries/game-engine/build/src/dependencies/yaml-cpp/
```

Or, so you don't have to re-add it every time you restart:
(modify the path to match your virtual machine if necessary)
```
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/aerial-robotics/Libraries/game-engine/build/src/dependencies/yaml-cpp/' >> ~/.zshrc
source ~/.zshrc
```

## Pre-Tournament 5/14

| Team Name                  | 1st Balloon Pop | 2nd Balloon Pop | Home    | Rank |
| ---                        | ---             | ---             | ---     |  --- |
| Drone Appetit              | 47.91           | 2:44.74         | -       | 6    |
| General Line               | 14.40           | 57.66           | 1:48.32 | 2    |
| Quadsadilla                | -               | -               | -       |      |
| Rolling Drones             | 15.87           | 1:29.35         | 2:37.97 | 4    |
| TEAM_NAME_HERE             | 16.40           | 38.51           | 50.18   | 1    |
| Todd's Quad Squad          | 22.26           | 1:05.06         | 1:58.29 | 3    |
| Unscented Kwadrotor Fliers | 1:09.06         | 2:26.53         | 2:54.99 | 5    |

'-' denotes a protocol that fails to reach the goal.

Results were taken from the best of 3 runs using the random seed 49.